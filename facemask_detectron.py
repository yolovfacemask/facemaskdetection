from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
import cv2
from detectron2 import model_zoo
cfg_1 = get_cfg()
cfg_1.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_R_101_DC5_3x.yaml"))
cfg_1.MODEL.WEIGHTS='/content/output/model_final.pth'
cfg_1.MODEL.ROI_HEADS.NUM_CLASSES = 2
cfg_1.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7 
predictor_1 = DefaultPredictor(cfg_1)

im=cv2.imread('/content/a3mw5y66m2y51.png')
output=predictor_1(im)
boxes=output['instances'].pred_boxes.tensor.cpu().numpy()
clases=output['instances'].pred_classes.cpu().numpy()
print(boxes)
print(clases)
for b,c in zip(boxes,clases):
  print(b,c)
  start_point=(int(b[0]),int(b[1]))
  end_point=(int(b[2]),int(b[3]))
  if c ==1:
    color=(0,0,255)
  else:
    color=(0,255,0)
  image1 = cv2.rectangle(im, start_point, end_point, color, 2)
cv2.imshow('result',image1)